// Aggregation Pipelines

db.collections.aggregate(
	[
		{stage 1},
		{stage 2}
	]

);

// stage 1
	// $match: {<field>: <value>}

// customer id 
db.orders.aggregate(
		[
			{$match: {"cust_id" : "A123"}}
		]
	);	

// customer id and status: paid (2 fields)
db.orders.aggregate(
		[
			{$match: {"cust_id" : "A123", "status" : "paid"}},
		]
	);	
	


// using logical operators, add status pending
db.orders.aggregate(
	[
		{$match: {
			$or: [
					{"cust_id" : "A123"}, 
					{"status" : "paid"},
					{"status" : "pending"}
				]

			}
		}
	]
);

// another aggregate pipeline
	// $project
		// same as field projection
		// $project: {<field> : <value>}

// hide customer id
db.orders.aggregate(
		[
			{$match: {"status" : "paid"}}
			{$project: {"cust_id": 0}}
		]
			
	);	

// hide customer id and status
db.orders.aggregate(
		[
			{$match: {"status" : "paid"}}
			{$project: {"cust_id": 0, "status": 0}}
		]
			
	);	

// another pipeline
	// $sort
	// $sort: {<field> : 1 or -1}

	db.orders.aggregate(
		[
			{$match: {"status" : "paid"}},
			{$sort: {"amount" : 1}},
			{$project: {"cust_id": 0, "status" : 0}}
		]
	);	


// another pipeline
	// $group: {_id: <$field>, key: {acumulator operator}}

db.orders.aggregate(
	[
		{$match: {"status" : "paid"}},
		{$group: {_id: "$cust_id", amount: {$sum: "$amount"}}},
		{$sort: {"amount" : -1}}
	]
);

